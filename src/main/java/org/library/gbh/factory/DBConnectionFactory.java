package org.library.gbh.factory;

import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;

@Slf4j
public class DBConnectionFactory {

    private DBConnectionFactory() {
    }

    public static DBConnection getDbConnection(DBTYPE type) throws SQLException {
       switch (type) {
           case ORACLE:
               return OracleConnection.getInstance();
           case MYSQL:
               return MysqlConnection.getInstance();
           default:
               throw new SQLException();
       }
   }
}