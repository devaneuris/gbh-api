package org.library.gbh.factory;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Slf4j
public class OracleConnection implements DBConnection {


    private Connection connection = null;
    private static OracleConnection instance;

    private OracleConnection() {

        final String cs = "jdbc:oracle:thin://localhost:1521:gbhlibrary";
        String user = "root";
        String password = "secret";

        try {
            this.connection = DriverManager.getConnection(cs, user, password);
            log.info("Connection class => " + this.connection.getClass().getCanonicalName());

        } catch (Exception e) {
            log.info("Error while created the connection: " + e.getMessage());
        }
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    public static OracleConnection getInstance() throws SQLException {
        if (instance == null || instance.getConnection().isClosed()) {
            instance = new OracleConnection();
        }

        return instance;
    }
}
