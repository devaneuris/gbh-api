package org.library.gbh.factory;

public enum DBTYPE {
    MYSQL,
    ORACLE,
    SQLSERVER
}
