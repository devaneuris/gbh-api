package org.library.gbh.factory;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Slf4j
public class MysqlConnection implements DBConnection {

    static {
        try {
            new com.mysql.cj.jdbc.Driver();
        } catch (SQLException cd) {
            //catch
        }
    }

    private Connection connection = null;
    private static MysqlConnection instance;

    private MysqlConnection() {

        final String cs = "jdbc:mysql://db:3306/gbhlibrary";
        String user = "root";
        String password = "secret";

        try {
            this.connection = DriverManager.getConnection(cs, user, password);
            log.info("Connection class => " + this.connection.getClass().getCanonicalName());

        } catch (Exception e) {
            log.info("Error while created the connection: " + e.getMessage());
        }
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    public static MysqlConnection getInstance() throws SQLException {
        if (instance == null || instance.getConnection().isClosed()) {
            instance = new MysqlConnection();
        }

        return instance;
    }
}
