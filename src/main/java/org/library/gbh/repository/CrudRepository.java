package org.library.gbh.repository;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {
    Optional<T> findById(Long id);
    List<T> findAll();
    void delete(Long id);
    T create(T object);
    Optional<T> update(T object);
}
