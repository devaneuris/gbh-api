package org.library.gbh.repository;

import lombok.extern.slf4j.Slf4j;
import org.library.gbh.factory.DBTYPE;
import org.library.gbh.model.Page;
import org.library.gbh.util.DaoSupport;
import org.library.gbh.factory.DBConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Slf4j
public class PageRepository {

    public Optional<Page> findByPage(Long bookId, int number, String format) {

        Page page = null;

        try (Connection conn = DBConnectionFactory.getDbConnection(DBTYPE.MYSQL).getConnection();
             PreparedStatement ps = DaoSupport.getPageParametrize(bookId, number, format, conn);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                page = new Page(rs.getLong("id"),
                        rs.getInt("page_number"),
                        rs.getString("content"),
                        rs.getInt("book_id"),
                        rs.getString("format")
                );
            }
        }

        catch (SQLException e) {
            log.info(e.getMessage());
        }

        return Optional.ofNullable(page);
    }
}
