package org.library.gbh.repository;

import java.util.List;
import java.util.Optional;

public interface ReadableRepository<T> {
    Optional<T> findById(Long id);
    List<T> findAll();
}
