package org.library.gbh.repository;

import java.util.Optional;

public interface WritableRepository<T> {
    T create(T object);
    Optional<T> update(T object);
}
