package org.library.gbh.repository;

import lombok.extern.slf4j.Slf4j;
import org.library.gbh.factory.DBTYPE;
import org.library.gbh.model.Book;
import org.library.gbh.util.DaoSupport;
import org.library.gbh.factory.DBConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class BookRepository implements ReadableRepository<Book> {

    @Override
    public Optional<Book> findById(Long id) {

        Book book = null;

        try (Connection conn = DBConnectionFactory.getDbConnection(DBTYPE.MYSQL).getConnection();
             PreparedStatement ps = DaoSupport.getBookParametrize(id, conn);
             ResultSet rs = ps.executeQuery();) {

            while (rs.next()) {
                book = new Book(rs.getLong("id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("genre"),
                        rs.getString("isbn")
                );
            }
        }
        catch (SQLException e) {
            log.info(e.getMessage());
        }
        return Optional.ofNullable(book);
    }

    @Override
    public List<Book> findAll() {

        List<Book> books = new ArrayList<>();

        try (Connection connection = DBConnectionFactory.getDbConnection(DBTYPE.MYSQL).getConnection();
             Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery("select * from books");)
        {

            while(rs.next()) {
                books.add(new Book(rs.getLong("id"),
                        rs.getString("title"),
                        rs.getString("author"),
                        rs.getString("genre"),
                        rs.getString("isbn")
                ));
            }
        }
        catch (SQLException e) {
            log.info(e.getMessage());
        }
        return books;
    }

}
