package org.library.gbh.repository;

public interface DestroyableRepository {
    void delete(Long id);
}
