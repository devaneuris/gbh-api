package org.library.gbh.service;

import lombok.NoArgsConstructor;
import org.library.gbh.model.Page;
import org.library.gbh.repository.PageRepository;

import java.util.Optional;

@NoArgsConstructor
public class PageService {

    private final PageRepository pageRepository = new PageRepository();

    public Optional<Page> findPage(Long bookId, int number, String format) {
        return pageRepository.findByPage(bookId, number, format);
    }
}
