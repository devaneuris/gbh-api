package org.library.gbh.service;

import lombok.NoArgsConstructor;
import org.library.gbh.model.Book;
import org.library.gbh.repository.BookRepository;

import java.util.List;
import java.util.Optional;

@NoArgsConstructor
public class BookService {

    private final BookRepository bookRepository = new BookRepository();

    public List<Book> books() {
        return bookRepository.findAll();
    }

    public Optional<Book> findBook(Long id) {

        return bookRepository.findById(id);
    }
}
