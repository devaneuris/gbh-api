package org.library.gbh.controller;

import org.library.gbh.dto.PageDto;
import org.library.gbh.model.Page;
import org.library.gbh.service.PageService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/book")
public class PageController {


    private final PageService pageService = new PageService();

    @Path("/{id}/page/{number}/{format}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") Long bookId, @PathParam("number") int number, @PathParam("format") String format) {

        Optional<Page> optional = pageService.findPage(bookId, number, format);

        if (!optional.isPresent()) {
            throw new NotFoundException(String.format("The book $0 can`t be found.", bookId));
        }

        PageDto page = new PageDto(optional.get().getPageNumber(), optional.get().getContent(), optional.get().getFormat());

        return Response.ok(page).build();
    }
}
