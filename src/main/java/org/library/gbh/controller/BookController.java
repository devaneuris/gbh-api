package org.library.gbh.controller;

import org.library.gbh.dto.BookDto;
import org.library.gbh.model.Book;
import org.library.gbh.service.BookService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("/book")
public class BookController {

    private final BookService bookService = new BookService();

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllBooks() {

        List<BookDto> books = bookService.books()
                .stream()
                .map(book -> new BookDto(book.getId(), book.getTitle(), book.getGenre()))
                .collect(Collectors.toList());

        return Response.ok(books).build();
    }

    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") Long id) {

        Optional<Book> optional = bookService.findBook(id);

        if (!optional.isPresent()) {
            throw new NotFoundException(String.format("The book %d can`t be found.", id));
        }

        BookDto book = new BookDto(optional.get().getId(), optional.get().getTitle(), optional.get().getGenre());

        return Response.ok(book).build();
    }
}
