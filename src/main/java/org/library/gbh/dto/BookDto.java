package org.library.gbh.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class BookDto {
    private final Long id;
    private final String title;
    private final String genre;
}
