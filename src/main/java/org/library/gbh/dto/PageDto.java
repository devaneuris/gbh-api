package org.library.gbh.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class PageDto {
    private final int pageNumber;
    private final String content;
    private final String format;
}
