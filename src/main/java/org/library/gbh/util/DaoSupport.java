package org.library.gbh.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DaoSupport {

    public static final String url = "jdbc:mysql://localhost:3306/gbhlibrary";
    public static final String username = "root";
    public static final String password = "secret";

    public static final String pageParametrize = "select * from pages where page_number = ? and book_id = ? and format = ?";
    public static final String bookParametrize = "select * from books where id = ?";

    public static PreparedStatement getPageParametrize(Long bookId, int number, String format, Connection connection) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(pageParametrize);
        ps.setInt(1, number);
        ps.setLong(2, bookId);
        ps.setString(3, format);
        return ps;
    }

    public static PreparedStatement getBookParametrize(Long id, Connection connection) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(bookParametrize);
        ps.setLong(1, id);
        return ps;
    }
}
