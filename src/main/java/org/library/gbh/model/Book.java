package org.library.gbh.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Book {

    private final Long id;
    private final String title;
    private final String author;
    private final String genre;
    private final String isbn;
}
