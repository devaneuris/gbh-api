package org.library.gbh.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Page {
    private final Long id;
    private final int pageNumber;
    private final String content;
    private final int bookId;
    private final String format;
}
