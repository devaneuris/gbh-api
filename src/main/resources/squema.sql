create database gbhlibrary;

use gbhlibrary;

create table books(
                      id int not null primary key,
                      title varchar(100) not null,
                      author varchar(20),
                      genre varchar(20),
                      isbn char(6) not null
)

create table pages(
                      id int not null primary key,
                      page_number int not null,
                      content varchar(5000),
                      book_id int,
                      format char(10)
)