INSERT INTO books (id, title, author, genre, isbn) values (1, 'War of the Worlds', 'H.G Wells', 'SciFi', '123474');
INSERT INTO books (id, title, author, genre, isbn) values (2, 'Da Vinci Code', 'Dan Brown', 'Thriller', '365287');
INSERT INTO books (id, title, author, genre, isbn) values (3, 'Inferno', 'Dan Brown', 'Thriller', '478554');
INSERT INTO books (id, title, author, genre, isbn) values (4, 'The Dark Forest', 'Cixin Liu', 'SciFi', '214578');


INSERT INTO pages(id, page_number, content, book_id, format) values (1, 1, 'Content page from book 1', 1, 'pdf')
INSERT INTO pages(id, page_number, content, book_id, format) values (2, 2, 'Content page from book 1', 1, 'html')
INSERT INTO pages(id, page_number, content, book_id, format) values (3, 1, 'Content page from book 1', 1, 'html')
INSERT INTO pages(id, page_number, content, book_id, format) values (4, 2, 'Content page from book 1', 1, 'pdf')
